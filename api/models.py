from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models
from datetime import datetime


class UserManager(BaseUserManager):
    def create_user(self, first_name, last_name, username, password, is_teacher, is_admin):
        if not username:
            raise ValueError('Users must have a username')

        user = self.model(
            username=username,
            first_name=first_name,
            last_name=last_name,
            is_admin=is_admin,
            is_teacher=is_teacher
        )

        user.set_password(password)
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=50)
    username = models.CharField(max_length=30, unique=True)
    password = models.CharField(max_length=100)
    is_admin = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['first_name', 'last_name', 'password']

    objects = UserManager()

    def __str__(self):
        return self.get_username()


class Task(models.Model):
    name = models.CharField(max_length=50)
    content = models.CharField(max_length=5000)
    example = models.CharField(max_length=1000)
    example_input = models.CharField(max_length=500)
    example_output = models.CharField(max_length=500)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()

    def __str__(self):
        return self.name


class Solution(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    language = models.CharField(max_length=10)
    source = models.CharField(max_length=10000)
    status = models.CharField(max_length=20)
    timestamp = models.DateTimeField(default=datetime.now)
    points = models.CharField(max_length=10)


class Test(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    input = models.CharField(max_length=500)
    output = models.CharField(max_length=500)
    max_time = models.FloatField()
    memory = models.IntegerField()
    points = models.IntegerField()


class Rating(models.Model):
    points = models.IntegerField()
    time = models.FloatField()
    output = models.CharField(max_length=300)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    solution = models.ForeignKey(Solution, on_delete=models.CASCADE)
    token = models.CharField(max_length=100)
    status = models.CharField(max_length=30)
    tries_left = models.IntegerField(default=10)


class News(models.Model):
    title = models.CharField(max_length=100)
    content = models.CharField(max_length=1000)
    author = models.CharField(max_length=50)
    timestamp = models.DateTimeField(default=datetime.now)


class TaskResult(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    solution_amount = models.IntegerField()
    result = models.FloatField()

