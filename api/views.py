from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from django.contrib.auth import login
from django.utils import timezone
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.authtoken.views import ObtainAuthToken
from .models import User, Task, Solution, News, Rating, Test, TaskResult
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect, render
import threading
import time


class RegistrationView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        username = request.data.get('username', '')
        password = request.data.get('password', '')
        first_name = request.data.get('first_name', '')
        last_name = request.data.get('last_name', '')
        is_teacher = request.data.get('is_teacher', '')

        if '' in [username, password, first_name, last_name, is_teacher]:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Nieprawidłowe dane'})
        try:
            user = User.objects.create_user(first_name=first_name, last_name=last_name, username=username,
                                            password=password, is_teacher=is_teacher, is_admin=False)
            user.save()
            token = str(Token.objects.create(user=user))
        except IntegrityError:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Podana nazwa użytkownika jest zajęta'})

        return Response(data={'token': token,
                              'is_teacher': is_teacher, 'first_name': first_name}, status=status.HTTP_200_OK)

    def get(self, request):
        return render(request, 'registration.html')


class CustomAuthToken(ObtainAuthToken):
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({
            'token': token.key,
            'first_name': user.first_name,
            'is_teacher': user.is_teacher
        })


class LoginPageView(APIView):
    def get(self, request):
        return render(request, 'login.html', {'error': False})


class LogoutView(APIView):
    def post(self, request):
        user = request.user
        username = request.data.get('username', '')
        password = request.data.get('password', '')

        if '' in [username, password]:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Nieprawidłowe dane'})


def base(request):
    return render(request, 'index.html')


@login_required
def index(request):
    news = News.objects.order_by('id').reverse()
    return render(request, 'index.html', {'news': news,
                                          'name': request.user.first_name + " " + request.user.last_name})


@csrf_exempt
def registration_view(request):
    data = request.POST
    if 'first_name' in data and 'last_name' in data and 'login' in data and 'password' in data and 'class' in data and 'num' in data:
        try:
            user = User.objects.create_user(data['first_name'], data['last_name'], data['login'], data['password'], data['class'], data['num'], False)
            user.save()
            login(request, user)
            return redirect('/contest')
        except IntegrityError as e:
            return render(request, 'registration.html', {'error': True})
    else:
        return render(request, 'registration.html', {'error': False})


@login_required
def regulations(request):
    return render(request, 'regulations.html', {'name': request.user.first_name + " " + request.user.last_name})


@login_required
def timetable(request):
    return render(request, 'calendar.html', {'name': request.user.first_name + " " + request.user.last_name})


@login_required
def task(request, task_id):
    try:
        _task = Task.objects.get(id=task_id)
        example_image = ""
        if _task.example:
            example_split = _task.example.split()
            if example_split[0] == "IMG":
                example_image = example_split[1]
        return render(request, 'task.html', {'task_name': _task.name,
                                             'is_active': _task.start_date < timezone.now(),
                                             'start_date': _task.start_date,
                                             'content': _task.content,
                                             'example': _task.example,
                                             'example_image': example_image,
                                             'example_input': _task.example_input,
                                             'example_output': _task.example_output,
                                             'end_date': _task.end_date,
                                             'name': request.user.first_name + " " + request.user.last_name})
    except ObjectDoesNotExist:
        return redirect('/contest')


RUN_URL = 'https://api.judge0.com/submissions/'
langs = ["Bash (4.4)", 'Bash (4.0)', 'Basic (fbc 1.05.0)', 'C (gcc 7.2.0)', 'C (gcc 6.4.0)', 'C (gcc 6.3.0)',
         'C (gcc 5.4.0)', 'C (gcc 4.9.4)', "C (gcc 4.8.5)", "C++ (g++ 7.2.0)", "C++ (g++ 6.4.0)", "C++ (g++ 6.3.0)",
         "C++ (g++ 5.4.0)", "C++ (g++ 4.9.4)", "C++ (g++ 4.8.5)", "C# (mono 5.4.0.167)", "C# (mono 5.2.0.224)",
         "Clojure (1.8.0)", "Crystal (0.23.1)", "Elixir (1.5.1)", "Erlang (OTP 20.0)", "Go (1.9)",
         "Haskell (ghc 8.2.1)", "Haskell (ghc 8.0.2)", "Insect (5.0.0)", "Java (OpenJDK 9 with Eclipse OpenJ9)",
         "Java (OpenJDK 8)", "Java (OpenJDK 7)", "JavaScript (nodejs 8.5.0)", "JavaScript (nodejs 7.10.1)",
         "OCaml (4.05.0)", "Octave (4.2.0)", "Pascal (fpc 3.0.0)", "Python (3.6.0)", "Python (3.5.3)", "Python (2.7.9)",
         "Python (2.6.9)", "Ruby (2.4.0)", "Ruby (2.3.3)", "Ruby (2.2.6)", "Ruby (2.1.9)", "Rust (1.20.0)",
         "Text (plain text)", "Executable"]


@login_required
def check_solution(request):
    data = request.POST
    try:
        _task = Task.objects.get(id=data['task'])
        if _task.end_date < timezone.now():
            return redirect('/contest')
        if Solution.objects.filter(user=request.user).filter(task=_task).count() > 4:
            return redirect('/contest')
    except ObjectDoesNotExist:
        return redirect('/contest')

    solution_object = Solution(user=request.user, task=_task, language=langs[int(data['lang']) - 1],
                               source=data['source'], status="Oczekuje", points="0 / 0")
    solution_object.save()

    tests = Test.objects.filter(task=_task)

    for test in tests:
        rating = Rating(solution=solution_object, points=0, time=0, test=test, status="Oczekuje", token="", output="-")
        rating.save()

    return redirect('/contest/solution/all')


@login_required
def solution_form(request):
    _tasks = Task.objects.filter(start_date__lte=timezone.now()).filter(end_date__gt=timezone.now())
    for _task in _tasks:
        if Solution.objects.filter(user=request.user).filter(task=_task).count() > 4:
            _tasks = _tasks.exclude(id=_task.id)

    return render(request, 'solutionform.html', {'tasks': _tasks,
                                                 'name': request.user.first_name + " " + request.user.last_name})


@login_required
def solutions_list(request):
    solutions = Solution.objects.filter(user=request.user).order_by('timestamp').reverse()
    for solution in solutions:
        solution.id = str(solution.id)
        if solution.task.end_date > timezone.now():
            solution.points = "0 / 0"
    return render(request, 'solutionlist.html', {'solutions': solutions,
                                                 'name': request.user.first_name + " " + request.user.last_name})


@login_required
def solutions_details(request, solution_id):
    solution = Solution.objects.get(id=solution_id)
    if solution.user != request.user:
        return redirect('/contest/solution/all')
    ratings = Rating.objects.filter(solution=solution_id)
    if solution.task.end_date > timezone.now():
        ratings = ratings.filter(test__points=0)
    return render(request, 'solutiondetails.html', {'solution': solution, 'ratings': ratings,
                                                    'name': request.user.first_name + " " + request.user.last_name})


@login_required
def tasks(request, part):
    max_id = part * 5
    min_id = part * 5 - 4
    _tasks = Task.objects.filter(id__gte=min_id).filter(id__lte=max_id)
    for _task in _tasks:
        _task.id = str(_task.id)
    return render(request, 'tasklist.html', {'part': part, 'tasks': _tasks})


@login_required
def results(request):
    _tasks = Task.objects.filter(end_date__lt=timezone.now())
    data = TaskResult.objects.filter(task__in=_tasks)
    data_to_show = []
    for user in User.objects.all().exclude(username='admin'):
        user_data = data.filter(user=user)
        data_to_insert = {'name': user.first_name + " " + user.last_name, 'suma': 0}
        for _task in _tasks:
            try:
                value = user_data.get(task=_task).result
            except ObjectDoesNotExist:
                value = 0
            data_to_insert[_task.id] = value
            data_to_insert['suma'] += value
        data_to_show.insert(0, data_to_insert)

    return render(request, 'results.html', {'data_to_show': data_to_show, 'tasks': _tasks,
                                            'name': request.user.first_name + " " + request.user.last_name})


@login_required
def results_admin(request):
    if request.user.username != 'admin':
        return redirect('/contest/results')
    _tasks = Task.objects.all()
    data = TaskResult.objects.filter(task__in=_tasks)
    data_to_show = []
    for user in User.objects.all().exclude(username='admin'):
        user_data = data.filter(user=user)
        data_to_insert = {'name': user.first_name + " " + user.last_name, 'suma': 0}
        for _task in _tasks:
            try:
                value = user_data.get(task=_task).result
            except ObjectDoesNotExist:
                value = 0
            data_to_insert[_task.id] = value
            data_to_insert['suma'] += value
        data_to_show.insert(0, data_to_insert)

    return render(request, 'results.html', {'data_to_show': data_to_show,
                                            'tasks': _tasks,
                                            'name': request.user.first_name + " " + request.user.last_name})


@login_required
def users_management_page(request):
    if not request.user.is_admin:
        redirect('/')
    users = User.objects.all()
    return render(request, 'admin_user_list.html', {'name': request.user.first_name + " " + request.user.last_name,
                                                    'users': users})


@login_required
def users_management_admin(request):
    if not request.user.is_admin:
        redirect('/user/profile')

    data = request.data

    if 'user_id' not in data or 'is_admin' not in data:
        redirect('/user/management')

    user = data.get('user_id', request.user)
    user.is_admin = data.get('is_admin', user.is_admin)
    user.save()
    redirect('/user/management')


@login_required
def profile_management(request):
    data = request.data
    user = data.user
    user.first_name = data.get('first_name', user.first_name)
    user.last_name = data.get('last_name', user.last_name)
    user.number = data.get('num', user.number)
    user.class_name = data.get('class_name', user.class_name)
    if 'new_password' in data:
        user.set_password(data.get('password'))
    user.save()
    redirect('/user/profile')


def get_solutions_results():
    while True:
        time.sleep(20)
        ratings = Rating.objects.filter(status="Zgłoszono")
        for rating in ratings:
            url = 'https://api.judge0.com/submissions/' + rating.token + '?fields=stdout,time,status_id'
            res = requests.get(url=url)

            rating.tries_left -= 1
            if rating.tries_left == 0:
                rating.status = "Błąd serwera"
            rating.save()

            if not res.status_code == 200:
                continue

            data = res.json()
            status = int(data['status_id'])
            if status < 3 or status == 13:
                pass
            elif status == 3:
                if data['stdout']:
                    rating.output = str(data['stdout']).rstrip()
                rating.time = data['time']

                if rating.output == rating.test.output:
                    if float(rating.time) > rating.test.max_time:
                        rating.points = 0
                        rating.status = "Przekroczono czas"
                    else:
                        rating.points = rating.test.points
                        rating.status = "OK"
                else:
                    rating.points = 0
                    rating.status = "Zła odpowiedź"
            elif status == 6:
                rating.status = "Błąd kompilacji"
            elif status == 5:
                rating.status = "Przekroczono limity"
            elif status == 13:
                rating.status = "Błąd serwera"
            else:
                rating.status = "Runtime error"
            rating.solution.status = "Sprawdzono"
            points = rating.solution.points.split('/')
            points[0] = int(points[0]) + rating.points
            points[1] = int(points[1]) + rating.test.points

            rating.solution.points = str(points[0]) + " / " + str(points[1])
            rating.save()
            rating.solution.save()

            if (points[1] == 100 and 6 > rating.test.task.id > 0) or points[1] == 200:
                try:
                    task_result = TaskResult.objects.get(user=rating.solution.user, task=rating.solution.task)
                    if task_result.solution_amount < 3:
                        if points[0] > task_result.result:
                            task_result.result = points[0]
                    elif task_result.solution_amount == 3:
                        task_result.result = (task_result.result + points[0]) / 2
                    else:
                        task_result.result = (task_result.result * 2 + points[0]) / 3
                    task_result.solution_amount += 1
                    task_result.save()
                except ObjectDoesNotExist:
                    TaskResult(user=rating.solution.user, task=rating.solution.task, solution_amount=1, result=points[0]).save()

        ratings2 = Rating.objects.filter(status="Oczekuje")
        for rating in ratings2:
            post_data = {
                'source_code': rating.solution.source,
                'language_id': langs.index(rating.solution.language) + 1,
                'stdin': rating.test.input,
                'cpu_time_limit': 15
            }

            rating.tries_left -= 1
            if rating.tries_left == 0:
                rating.status = "Błąd serwera"

            res = requests.post(RUN_URL, data=post_data)

            if 'token' in res.json():
                rating.status = "Zgłoszono"
                rating.token = res.json()['token']

            rating.save()


# thread = threading.Thread(target=get_solutions_results, args=())
# thread.daemon = True
# thread.start()
