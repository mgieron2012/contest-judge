from django.urls import path
from . import views
from .views import CustomAuthToken

urlpatterns = [
    path('/', views.base, name="index"),
    path('login/', views.LoginPageView.as_view(), name='login'),
    path('login/token/', CustomAuthToken.as_view(), name='login_token'),
    path('registration/', views.RegistrationView.as_view(), name='registration'),
    path('regulations', views.regulations, name='regulations'),
    path('timetable', views.timetable, name='timetable'),
    path('task/<int:task_id>', views.task, name='task'),
    path('tasks/<int:part>', views.tasks, name='tasks'),
    path('solution/details/<int:solution_id>', views.solutions_details, name='solution_details'),
    path('solution/check', views.check_solution, name='check_solution'),
    path('solution/all', views.solutions_list, name='solutions_list'),
    path('solution/', views.solution_form, name='solution_form'),
    path('results/', views.results, name='results'),
    path('results/admin', views.results_admin, name='results_admin'),
    path('user/management', views.users_management_page, name='user_management'),
]
