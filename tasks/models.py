from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.utils import timezone

from api.models import User
from group.models import Group


class Task(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="task_owner", blank=True)
    title = models.CharField(max_length=250)
    content = models.CharField(max_length=2000)
    languages = ArrayField(models.IntegerField())
    model_example = models.CharField(max_length=10000, blank=True)
    example_language = models.CharField(max_length=10, blank=True)
    example_purpose = models.IntegerField()


class Test(models.Model):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)
    input_data = models.CharField(max_length=5000, blank=True)
    output_data = models.CharField(max_length=3000, blank=True)
    points = models.IntegerField(default=0)
    checking_method = models.IntegerField(default=0)


class Work(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE, related_name="work_group", blank=True)
    task = models.ForeignKey(Task, on_delete=models.CASCADE, related_name="work_task", blank=True)
    deadline = models.DateTimeField()
    expiration_time = models.DateTimeField()


class Solution(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="solution_owner", blank=True)
    work = models.ForeignKey(Work, on_delete=models.CASCADE, related_name="solution_work")
    timestamp = models.DateTimeField(default=timezone.now)
    code = models.CharField(max_length=20000)
    language = models.IntegerField()
    grade = models.CharField(max_length=25, blank=True)


class TestResult(models.Model):
    solution = models.ForeignKey(Solution, on_delete=models.CASCADE)
    test = models.ForeignKey(Test, on_delete=models.CASCADE)
    output = models.CharField(max_length=1000, blank=True)
    is_result_correct = models.BooleanField(null=True)
    checked = models.BooleanField(default=False)
