from rest_framework import serializers

from api.serializers import UserSerializer
from group.serializers import GroupNameSerializer
from tasks.models import Task, Test, Work, Solution


class TaskSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)

    class Meta:
        model = Task
        fields = ['pk', 'owner', 'title', 'content',
                  'languages', 'model_example', 'example_language', 'example_purpose']


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = ['pk', 'task', 'input_data', 'output_data', 'points', 'checking_method']


class TaskWithoutExampleSerializer(serializers.ModelSerializer):
    test_set = TestSerializer(read_only=True, many=True)

    class Meta:
        model = Task
        fields = ['pk', 'owner', 'title', 'content', 'languages', 'test_set']


class WorkSerializer(serializers.ModelSerializer):
    group = GroupNameSerializer(read_only=True)
    task = TaskWithoutExampleSerializer(read_only=True)

    class Meta:
        model = Work
        fields = ['pk', 'group', 'task', 'deadline', 'expiration_time']


class SolutionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solution
        fields = ['pk', 'owner', 'work', 'code', 'language']


class SolutionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Solution
        fields = ['pk', 'owner', 'work', 'code', 'language', 'grade']

