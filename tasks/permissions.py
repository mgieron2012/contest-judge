from rest_framework import permissions


class OwnerPermission(permissions.BasePermission):
    message = "Only task owner can perform this action"

    def has_object_permission(self, request, view, obj):
        return request.user == obj.owner


class TeacherPermission(permissions.BasePermission):
    message = "Only teacher's account can perform this action"

    def has_permission(self, request, view):
        return request.user.is_teacher


class GroupMemberPermission(permissions.BasePermission):
    message = "Available only for group members"

    def has_object_permission(self, request, view, obj):
        return request.user in obj.users.all() or request.user == obj.owner
