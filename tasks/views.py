from django.core.exceptions import ObjectDoesNotExist
from rest_framework import generics, status, filters
from rest_framework.exceptions import ValidationError
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from group.models import Group
from tasks.models import Task, Test, Work, Solution, TestResult
from tasks.permissions import OwnerPermission, TeacherPermission
from tasks.serializers import TaskSerializer, TestSerializer, WorkSerializer, SolutionCreateSerializer, \
    TaskWithoutExampleSerializer


class ListCreateTaskView(generics.ListCreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, TeacherPermission]
    pagination_class = None
    filter_backends = [filters.OrderingFilter]
    ordering = ['title']

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def get_queryset(self):
        return Task.objects.filter(owner=self.request.user)


class TaskView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, OwnerPermission]


class ListTestView(generics.ListCreateAPIView):
    queryset = Test.objects.all()
    serializer_class = TestSerializer

    def get_queryset(self):
        if 'task' in self.request.query_params:
            return Test.objects.filter(task=self.request.query_params['task'])
        else:
            raise ValidationError


class ManageTestView(APIView):
    def post(self, request):
        data = request.data
        if 'task' not in data or 'tests' not in data:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            task = Task.objects.get(pk=data['task'])
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        if request.user != task.owner:
            return Response(status=status.HTTP_403_FORBIDDEN)

        for test_data in data['tests'].values():
            if int(test_data['pk']) < 0:
                # create new test
                if test_data['deleteMe'] == "true":
                    continue
                del test_data['pk']
                test_data['task'] = task.pk
                serializer = TestSerializer(data=test_data)
                if serializer.is_valid(raise_exception=True):
                    serializer.save()
            else:
                try:
                    test = Test.objects.get(pk=test_data['pk'])
                except ObjectDoesNotExist:
                    return Response(status=status.HTTP_400_BAD_REQUEST)
                if test_data['deleteMe'] == "true":
                    # delete test
                    test.delete()
                else:
                    # update test
                    serializer = TestSerializer(test, data=test_data, partial=True)
                    if serializer.is_valid(raise_exception=True):
                        serializer.save()
        return Response(status=status.HTTP_200_OK,
                        data=TestSerializer(Test.objects.filter(task=task), many=True).data)


class CreateWorkView(generics.CreateAPIView):
    queryset = Work.objects.all()
    serializer_class = WorkSerializer

    def perform_create(self, serializer):
        try:
            group = Group.objects.get(pk=self.request.data['group_id'])
            task = Task.objects.get(pk=self.request.data['task_id'])
        except ObjectDoesNotExist:
            raise ValidationError
        if group.owner != self.request.user:
            raise ValidationError
        try:
            Work.objects.get(group=group, task=task)
            raise ValidationError
        except ObjectDoesNotExist:
            serializer.save(group=group, task=task)


class ListWorkView(generics.ListAPIView):
    queryset = Work.objects.all()
    serializer_class = WorkSerializer
    permission_classes = [IsAuthenticated]
    filter_backends = [filters.OrderingFilter]
    ordering = ['-deadline']

    def get_queryset(self):
        user = self.request.user
        if user.is_teacher:
            return Work.objects.filter(group__owner=user)
        else:
            return Work.objects.filter(group__users=user)


class GetWorkView(generics.RetrieveAPIView):
    queryset = Work.objects.all()
    serializer_class = WorkSerializer


class CreateSolutionView(generics.CreateAPIView):
    queryset = Solution.objects.all()
    serializer_class = SolutionCreateSerializer

    def perform_create(self, serializer):
        try:
            work = Work.objects.get(pk=serializer.data['work'])
        except ObjectDoesNotExist:
            raise ValidationError
        if serializer.data['language'] not in work.task.languages or self.request.user not in work.group.users:
            raise ValidationError
        solution = serializer.save(owner=self.request.user)
        for test in Test.objects.filter(task=work.task):
            TestResult(solution=solution, test=test).save()


class GetTaskWithTestsView(generics.RetrieveAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskWithoutExampleSerializer
