from django.urls import path
from . import views

urlpatterns = [
    path('', views.ListCreateTaskView.as_view(), name="tasks"),
    path('<int:pk>/', views.TaskView.as_view(), name="task_retrieve"),
    path('tests/list/', views.ListTestView.as_view(), name="tests_list"),
    path('tests/', views.ManageTestView.as_view(), name="tests"),
    path('work/', views.CreateWorkView.as_view(), name="work"),
    path('work/list/', views.ListWorkView.as_view(), name="work"),
    path('work/solution/', views.CreateSolutionView.as_view(), name="solution_create"),
]
