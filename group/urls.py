from django.urls import path
from . import views

urlpatterns = [
    path('', views.GroupView.as_view(), name="group"),
    path('details/<int:pk>/', views.GroupRetrieveView.as_view(), name="group_retrieve"),
    path('<int:pk>/', views.ManageGroupOwnerView.as_view(), name="group_manage"),
    path('token/', views.TokenView.as_view(), name="token"),
]
