from api.serializers import UserSerializer
from group.models import Group, GroupJoinToken
from rest_framework import serializers


class GroupSerializer(serializers.ModelSerializer):
    owner = UserSerializer(read_only=True)
    users = UserSerializer(many=True, read_only=True)

    class Meta:
        model = Group
        fields = ['pk', 'owner', 'name', 'users']


class GroupNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['pk', 'name']


class GroupTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = GroupJoinToken
        fields = ['token', 'group', 'expiration_time']

    def create(self, validated_data):
        return GroupJoinToken.objects.create(**validated_data)

    def update(self):
        pass
