from django.http import Http404
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from api.models import User
from .permissions import TeacherPermission, GroupOwnerPermission, GroupMemberPermission
from .serializers import GroupSerializer, GroupTokenSerializer
from .models import Group, GroupJoinToken
from rest_framework import generics


class GroupView(generics.ListCreateAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [IsAuthenticated, GroupOwnerPermission, TeacherPermission]
    pagination_class = []

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    def list(self, request, *args, **kwargs):
        if request.user.is_teacher:
            queryset = self.get_queryset().filter(owner=request.user)
        else:
            queryset = self.get_queryset().filter(users=request.user)

        serializer = GroupSerializer(queryset, many=True)
        return Response(serializer.data)


class GroupRetrieveView(generics.RetrieveAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [GroupMemberPermission]


class ManageGroupOwnerView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [GroupOwnerPermission]

    def perform_update(self, serializer):
        if 'user_to_remove' in self.request.data:
            try:
                user = User.objects.get(pk=self.request.data['user_to_remove'])
                if (group := self.get_object()) in user.group_set.all():
                    group.users.remove(user)
                else:
                    raise Http404
            except ObjectDoesNotExist:
                raise Http404
        serializer.save()


class TokenView(generics.CreateAPIView, generics.UpdateAPIView):
    queryset = GroupJoinToken.objects.all()
    serializer_class = GroupTokenSerializer
    permission_classes = [TeacherPermission, GroupOwnerPermission]

    def get_object(self):
        return GroupJoinToken.objects.get(token=self.request.data.get('token', ''))

    def put(self, request, *args, **kwargs):
        try:
            token = self.get_object()
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"error": "Podany token jest nieprawidłowy"})

        if token.expiration_time < timezone.now():
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"error": "Podany token stracił ważność"})

        group = token.group

        if group in request.user.group_set.all() or group.owner == request.user:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={"error": "Użytkownik jest już w tej grupie"})
        group.users.add(request.user)
        return Response(status=status.HTTP_200_OK, data=GroupSerializer(group).data)
