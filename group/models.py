from datetime import timedelta
from random import randint
from django.utils import timezone
from django.db import models
from api.models import User


def generate_token():
    return str(randint(100000, 999999))


def set_expiration_time():
    return timezone.now().__add__(timedelta(minutes=15))


class Group(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="owner", blank=True)
    name = models.CharField(max_length=150)
    users = models.ManyToManyField(User, blank=True)


class GroupJoinToken(models.Model):
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    token = models.CharField(max_length=6, default=generate_token, unique=True)
    expiration_time = models.DateTimeField(default=set_expiration_time)
