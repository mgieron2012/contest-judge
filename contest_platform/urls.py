from django.urls import include, path

urlpatterns = [
    path('', include('api.urls')),
    path('group/', include('group.urls')),
    path('tasks/', include('tasks.urls')),
]
